/**
 * @NApiVersion 2.1
 * @NScriptType Suitelet
 */
define(['N/record', 'N/redirect', 'N/ui/serverWidget', 'N/search', 'N/ui/message'],
    /**
 * @param{record} record
 */
    (record, redirect, serverWidget, search, message) => {
        /**
         * Defines the Suitelet script trigger point.
         * @param {Object} scriptContext
         * @param {ServerRequest} scriptContext.request - Incoming request
         * @param {ServerResponse} scriptContext.response - Suitelet response
         * @since 2015.2
         */
        const onRequest = (context) => {
            switch (context.request.method) {
                case 'GET': {
                    let form = serverWidget.createForm({ title: 'IEPS Trasladado' });
                    let sublist = createSublist(form);

                    form.addFieldGroup({ id: 'search_fields', label: 'Filtros de busqueda' });
                    [
                        { id: 'custpage_cha_list_subsidiary', type: serverWidget.FieldType.SELECT, label: 'Subsidiary', source: 'subsidiary', is_mandatory: true },
                        { id: 'custpage_cha_list_start_date', type: serverWidget.FieldType.DATE, label: 'Start Date' },
                        { id: 'custpage_cha_list_end_date', type: serverWidget.FieldType.DATE, label: 'End Date' },
                        // { id: 'custpage_cha_list_tax_code', type: serverWidget.FieldType.SELECT, label: 'Tax Code', source: 'salestaxitem', is_mandatory: true },
                    ].map((el) => {
                        let field = form.addField(el)
                        if (el.hasOwnProperty('is_mandatory') && el.is_mandatory) field.isMandatory = true;
                    })
                    form.addSubmitButton({ label: 'Generar Busqueda' });
                    context.response.writePage(form);
                    break;
                }
                case 'POST': {
                    let params = context.request.parameters;
                    log.debug('params', params)
                    let submitter = params['submitter']
                    log.debug('submitter', submitter)
                    switch (submitter) {
                        case 'Generar Busqueda': {
                            let form = serverWidget.createForm({ title: 'Conversion de solicitud de Cotización' });
                            let sublist = createSublist(form);

                            form.addFieldGroup({ id: 'search_fields', label: 'Filtros de busqueda' });
                            [
                                { id: 'custpage_cha_list_subsidiary', type: serverWidget.FieldType.SELECT, label: 'Subsidiary', source: 'subsidiary', is_mandatory: true },
                                { id: 'custpage_cha_list_start_date', type: serverWidget.FieldType.DATE, label: 'Start Date' },
                                { id: 'custpage_cha_list_end_date', type: serverWidget.FieldType.DATE, label: 'End Date' },
                                // { id: 'custpage_cha_list_tax_code', type: serverWidget.FieldType.SELECT, label: 'Tax Code', source: 'salestaxitem', is_mandatory: true },

                            ].map((el) => {
                                let field = form.addField(el)
                                if (el.hasOwnProperty('default_value')) field.defaultValue = el.default_value;
                            })

                            let data = getDataRequestQuote(params);
                            log.debug('data busqueda', data);

                            data.map((el, index) => {
                                let existLocation = el.location
                                let existDepartment = el.department;
                                let existClient = el.altname;
                                let existRfcClient = el.custentity_bex_rfc_taxid_nif;
                                let existproductDescription = el.productDescription;
                                let existproductType = el.productType;
                                let existgenericKey = el.genericKey;
                                let existgraduation = el.graduation;
                                let existquantity = el.quantity;
                                let existunits = el.units;
                                if (el.type === 'SalesOrd') {
                                    sublist.requestquoteSublist.setSublistValue({ id: 'custpage_cha_sub_type', value: 'C', line: index })
                                } else {
                                    sublist.requestquoteSublist.setSublistValue({ id: 'custpage_cha_sub_type', value: 'P', line: index })
                                }
                                sublist.requestquoteSublist.setSublistValue({ id: 'custpage_cha_sub_invoice', value: el.tranid || 'NUll', line: index })
                                sublist.requestquoteSublist.setSublistValue({ id: 'custpage_cha_sub_serie', value: el.transactionnumber || 'NUll', line: index })
                                sublist.requestquoteSublist.setSublistValue({ id: 'custpage_cha_sub_transaction', value: el.transactionnumber || 'NUll', line: index })
                                sublist.requestquoteSublist.setSublistValue({ id: 'custpage_cha_sub_document', value: 'Null', line: index })
                                sublist.requestquoteSublist.setSublistValue({ id: 'custpage_cha_sub_operation_id', value: el.custbody_bex_operation_number || 'NUll', line: index })
                                sublist.requestquoteSublist.setSublistValue({ id: 'custpage_cha_sub_date', value: el.trandate || 'NUll', line: index })
                                if (existLocation.length > 0) { sublist.requestquoteSublist.setSublistValue({ id: 'custpage_cha_sub_location', value: el.location || 'NUll', line: index }) }
                                if (existDepartment.length > 0) { sublist.requestquoteSublist.setSublistValue({ id: 'custpage_cha_sub_department', value: el.department || 'NUll', line: index }) }
                                if (existClient.length > 0) { sublist.requestquoteSublist.setSublistValue({ id: 'custpage_cha_sub_client', value: el.altname || 'NUll', line: index }) }
                                if (existRfcClient.length > 0) { sublist.requestquoteSublist.setSublistValue({ id: 'custpage_cha_sub_rfc', value: el.custentity_bex_rfc_taxid_nif || 'NUll', line: index }) }
                                sublist.requestquoteSublist.setSublistValue({ id: 'custpage_cha_sub_invoice_status', value: el.statusTran || 'NUll', line: index })
                                sublist.requestquoteSublist.setSublistValue({ id: 'custpage_cha_sub_state_key', value: el.city || 'NULL', line: index })
                                if (existproductDescription.length > 0) { sublist.requestquoteSublist.setSublistValue({ id: 'custpage_cha_sub_item_description', value: el.productDescription || 'NUll', line: index }) }
                                if (existproductType.length > 0) { sublist.requestquoteSublist.setSublistValue({ id: 'custpage_cha_sub_item_type', value: el.productType || 'NUll', line: index }) }
                                if (existgenericKey.length > 0) { sublist.requestquoteSublist.setSublistValue({ id: 'custpage_cha_sub_generic_key', value: el.genericKey || 'NUll', line: index }) }
                                if (existgraduation.length > 0) { sublist.requestquoteSublist.setSublistValue({ id: 'custpage_cha_sub_graduation', value: el.graduation || 'NUll', line: index }) }
                                if (existquantity.length > 0) { sublist.requestquoteSublist.setSublistValue({ id: 'custpage_cha_sub_amount', value: el.quantity || 'NUll', line: index }) }
                                if (existunits.length > 0) { sublist.requestquoteSublist.setSublistValue({ id: 'custpage_cha_sub_units', value: el.units || 'NUll', line: index }) }
                                sublist.requestquoteSublist.setSublistValue({ id: 'custpage_cha_sub_units_sat', value: 'NUll', line: index })
                                sublist.requestquoteSublist.setSublistValue({ id: 'custpage_cha_sub_units_packing', value: 'NUll', line: index })
                                sublist.requestquoteSublist.setSublistValue({ id: 'custpage_cha_sub_units_presentation', value: el.productType || 'NUll', line: index })
                                sublist.requestquoteSublist.setSublistValue({ id: 'custpage_cha_sub_units_subtotal', value: el.subtotal || 'NUll', line: index })
                                sublist.requestquoteSublist.setSublistValue({ id: 'custpage_cha_sub_units_iva', value: el.iva || 'NUll', line: index })
                                sublist.requestquoteSublist.setSublistValue({ id: 'custpage_cha_sub_units_ieps_rate', value: el.tasaIeps || 'NUll', line: index })
                                sublist.requestquoteSublist.setSublistValue({ id: 'custpage_cha_sub_units_ieps', value: el.ieps || 'NUll', line: index })
                                sublist.requestquoteSublist.setSublistValue({ id: 'custpage_cha_sub_units_discount_taxes', value: el.discount || 'NUll', line: index })
                                sublist.requestquoteSublist.setSublistValue({ id: 'custpage_cha_sub_units_discount_iva', value: el.ivadiscount || 'NUll', line: index })
                                sublist.requestquoteSublist.setSublistValue({ id: 'custpage_cha_sub_units_discount_ieps', value: el.iepsdiscount || 'NUll', line: index })
                                sublist.requestquoteSublist.setSublistValue({ id: 'custpage_cha_sub_units_net_invoice', value: el.netofactura || 'NUll', line: index })
                                sublist.requestquoteSublist.setSublistValue({ id: 'custpage_cha_sub_units_date_pay_invoice', value: el.fechapago || 'NUll', line: index })
                                sublist.requestquoteSublist.setSublistValue({ id: 'custpage_cha_sub_units_check_number', value: el.numerocheque || 'NUll', line: index })
                                sublist.requestquoteSublist.setSublistValue({ id: 'custpage_cha_sub_units_check_status', value: el.estatus || 'NUll', line: index })
                                sublist.requestquoteSublist.setSublistValue({ id: 'custpage_cha_sub_units_pay_type', value: el.tipopago || 'NUll', line: index })
                                sublist.requestquoteSublist.setSublistValue({ id: 'custpage_cha_sub_units_policy', value: el.poliza || 'NUll', line: index })
                                sublist.requestquoteSublist.setSublistValue({ id: 'custpage_cha_sub_units_policy_date', value: 'NUll', line: index })
                            });

                            form.addSubmitButton({ label: 'Generar Ordenes de Compra' });
                            context.response.writePage(form);
                            break;
                        }
                    }
                    break;
                }
            }
        }

        return { onRequest }

        //FUNCIONES
        function createSublist(form) {
            let requestquoteSublist = form.addSublist({ id: 'custpage_ges_sublist_item', type: serverWidget.SublistType.LIST, label: 'Resultado IEPS Trasladado' });
            [
                { id: 'custpage_cha_sub_type', type: serverWidget.FieldType.TEXT, label: 'Tipo', display_type: serverWidget.FieldDisplayType.INLINE },
                { id: 'custpage_cha_sub_invoice', type: serverWidget.FieldType.TEXT, label: 'Folio', display_type: serverWidget.FieldDisplayType.INLINE },
                { id: 'custpage_cha_sub_serie', type: serverWidget.FieldType.TEXT, label: 'Serie', display_type: serverWidget.FieldDisplayType.INLINE },
                { id: 'custpage_cha_sub_transaction', type: serverWidget.FieldType.TEXT, label: 'Transacción', display_type: serverWidget.FieldDisplayType.INLINE },
                { id: 'custpage_cha_sub_document', type: serverWidget.FieldType.TEXT, label: 'Documento', display_type: serverWidget.FieldDisplayType.INLINE },
                { id: 'custpage_cha_sub_operation_id', type: serverWidget.FieldType.TEXT, label: 'ID de operación', display_type: serverWidget.FieldDisplayType.INLINE },
                { id: 'custpage_cha_sub_date', type: serverWidget.FieldType.TEXT, label: 'Fecha', display_type: serverWidget.FieldDisplayType.INLINE },
                { id: 'custpage_cha_sub_location', type: serverWidget.FieldType.TEXT, label: 'Ubicación', display_type: serverWidget.FieldDisplayType.INLINE },
                { id: 'custpage_cha_sub_department', type: serverWidget.FieldType.TEXT, label: 'Departamento', display_type: serverWidget.FieldDisplayType.INLINE },
                { id: 'custpage_cha_sub_client', type: serverWidget.FieldType.TEXT, label: 'Cliente', display_type: serverWidget.FieldDisplayType.INLINE },
                { id: 'custpage_cha_sub_rfc', type: serverWidget.FieldType.TEXT, label: 'RFC', display_type: serverWidget.FieldDisplayType.INLINE },
                { id: 'custpage_cha_sub_invoice_status', type: serverWidget.FieldType.TEXT, label: 'Status factura', display_type: serverWidget.FieldDisplayType.INLINE },
                { id: 'custpage_cha_sub_state_key', type: serverWidget.FieldType.TEXT, label: 'Clave entidad federativa', display_type: serverWidget.FieldDisplayType.INLINE },
                { id: 'custpage_cha_sub_item_description', type: serverWidget.FieldType.TEXT, label: 'Descripcion del producto', display_type: serverWidget.FieldDisplayType.INLINE },
                { id: 'custpage_cha_sub_item_type', type: serverWidget.FieldType.TEXT, label: 'Tipo de producto', display_type: serverWidget.FieldDisplayType.INLINE },
                { id: 'custpage_cha_sub_generic_key', type: serverWidget.FieldType.TEXT, label: 'Clave generica', display_type: serverWidget.FieldDisplayType.INLINE },
                { id: 'custpage_cha_sub_graduation', type: serverWidget.FieldType.TEXT, label: 'Graduacion', display_type: serverWidget.FieldDisplayType.INLINE },
                { id: 'custpage_cha_sub_amount', type: serverWidget.FieldType.TEXT, label: 'Cantidad', display_type: serverWidget.FieldDisplayType.INLINE },
                { id: 'custpage_cha_sub_units', type: serverWidget.FieldType.TEXT, label: 'Unidades', display_type: serverWidget.FieldDisplayType.INLINE },
                { id: 'custpage_cha_sub_units_sat', type: serverWidget.FieldType.TEXT, label: 'Unidades SAT', display_type: serverWidget.FieldDisplayType.INLINE },
                { id: 'custpage_cha_sub_units_packing', type: serverWidget.FieldType.TEXT, label: 'Empaque', display_type: serverWidget.FieldDisplayType.INLINE },
                { id: 'custpage_cha_sub_units_presentation', type: serverWidget.FieldType.TEXT, label: 'Presentacion', display_type: serverWidget.FieldDisplayType.INLINE },
                { id: 'custpage_cha_sub_units_subtotal', type: serverWidget.FieldType.TEXT, label: 'Subtotal', display_type: serverWidget.FieldDisplayType.INLINE },
                { id: 'custpage_cha_sub_units_iva', type: serverWidget.FieldType.TEXT, label: 'IVA', display_type: serverWidget.FieldDisplayType.INLINE },
                { id: 'custpage_cha_sub_units_ieps_rate', type: serverWidget.FieldType.TEXT, label: 'Tasa ieps', display_type: serverWidget.FieldDisplayType.INLINE },
                { id: 'custpage_cha_sub_units_ieps', type: serverWidget.FieldType.TEXT, label: 'Ieps', display_type: serverWidget.FieldDisplayType.INLINE },
                { id: 'custpage_cha_sub_units_discount_taxes', type: serverWidget.FieldType.TEXT, label: 'Descuentos antes de impuestos', display_type: serverWidget.FieldDisplayType.INLINE },
                { id: 'custpage_cha_sub_units_discount_iva', type: serverWidget.FieldType.TEXT, label: 'IVA descuentos', display_type: serverWidget.FieldDisplayType.INLINE },
                { id: 'custpage_cha_sub_units_discount_ieps', type: serverWidget.FieldType.TEXT, label: 'IEPS descuentos', display_type: serverWidget.FieldDisplayType.INLINE },
                { id: 'custpage_cha_sub_units_net_invoice', type: serverWidget.FieldType.TEXT, label: 'Neto factura', display_type: serverWidget.FieldDisplayType.INLINE },
                { id: 'custpage_cha_sub_units_date_pay_invoice', type: serverWidget.FieldType.TEXT, label: 'Fecha pago factura', display_type: serverWidget.FieldDisplayType.INLINE },
                { id: 'custpage_cha_sub_units_check_number', type: serverWidget.FieldType.TEXT, label: 'Número de cheque', display_type: serverWidget.FieldDisplayType.INLINE },
                { id: 'custpage_cha_sub_units_check_status', type: serverWidget.FieldType.TEXT, label: 'Status cheque', display_type: serverWidget.FieldDisplayType.INLINE },
                { id: 'custpage_cha_sub_units_pay_type', type: serverWidget.FieldType.TEXT, label: 'Tipo de pago', display_type: serverWidget.FieldDisplayType.INLINE },
                { id: 'custpage_cha_sub_units_policy', type: serverWidget.FieldType.TEXT, label: 'Poliza', display_type: serverWidget.FieldDisplayType.INLINE },
                { id: 'custpage_cha_sub_units_policy_date', type: serverWidget.FieldType.TEXT, label: 'Fecha de poliza', display_type: serverWidget.FieldDisplayType.INLINE },
            ].map((el) => {
                let field = requestquoteSublist.addField(el);
                if (el.hasOwnProperty('display_type')) field.updateDisplayType({ displayType: el.display_type });
                if (el.hasOwnProperty('default_value')) field.defaultValue = el.default_value;

            });

            return { requestquoteSublist }

        }

        function getDataRequestQuote(params) {
            log.debug('params search', params)
            let filters = [
                ["type", "anyof", "SalesOrd", "PurchOrd"],
                "AND",
                ["trandate", "within", params['custpage_cha_list_start_date'], params['custpage_cha_list_end_date']],
                "AND",
                ["subsidiary", "anyof", params['custpage_cha_list_subsidiary']],
                "AND",
                ["mainline", "is", "T"]
            ];

            log.debug('filtros finales', filters)

            let info = []
            let requestforquoteSearchObj = search.create({
                type: "transaction",
                filters: filters,
                columns:
                    [
                        type = search.createColumn({ name: "type", label: "Tipo" }),
                        tranid = search.createColumn({ name: "tranid", label: "Número de documento" }),
                        transactionnumber = search.createColumn({ name: "internalid", label: "Internal ID" }),
                        search.createColumn({ name: "tranid", label: "Número de documento" }),
                        custbody_bex_operation_number = search.createColumn({ name: "custbody_bex_operation_number", label: "Operation Number" }),
                        trandate = search.createColumn({ name: "trandate", label: "Fecha" }),
                        loc = search.createColumn({ name: "location", label: "Ubicación" }),
                        department = search.createColumn({ name: "department", label: "Departamento" }),
                        altname = search.createColumn({ name: "altname", join: "customer", label: "nameCliente" }),
                        custentity_bex_rfc_taxid_nif = search.createColumn({ name: "custentity_bex_rfc_taxid_nif", join: "customer", label: "RFC / TAX ID o NIF" }),
                        statusTran = search.createColumn({ name: "statusref", label: "Estado" }),
                        city = search.createColumn({ name: "city", join: "location", label: "Ciudad" }),
                        productDescription = search.createColumn({ name: "salesdescription", join: "item", label: "Descripción" }),
                        productType = search.createColumn({ name: "type", join: "item", label: "Tipo" }),
                        genericKey = search.createColumn({ name: "tranid", label: "Número de documento" }),
                        quantity = search.createColumn({ name: "quantity", label: "Cantidad" }),
                        search.createColumn({ name: "custbody_fam_lp_annualrate", label: "Tasa de interés anual" }),
                        search.createColumn({ name: "amount", label: "Importe" }),
                        units = search.createColumn({ name: "unit", label: "Unidades" }),
                        subtotal = search.createColumn({ name: "netamountnotax", label: "Amount (Net of Tax)" }),
                        iva = search.createColumn({ name: "taxtotal", label: "Amount (Transaction Tax Total)" }),
                        tasaIeps = search.createColumn({ name: "custcol_be_ipestasa", label: "Tasa IEPS" }),
                        ieps = search.createColumn({ name: "custcol_cha_monto_ipes", label: "Monto IEPS" }),
                        discount = search.createColumn({ name: "discountamount", label: "Amount Discount" }),
                        ivadiscount = search.createColumn({ name: "amount", label: "Amount" }),
                        iepsdiscount = search.createColumn({ name: "amount", label: "Amount" }),
                        netofactura = search.createColumn({ name: "netamount", label: "Amount (Net)" }),
                        fechapago = search.createColumn({ name: "billeddate", label: "Date Billed" }),
                        numerocheque = search.createColumn({ name: "custbody_bex_operation_number", label: "Operation Number" }),
                        estatus = search.createColumn({ name: "statusref", label: "Status" }),
                        tipopago = search.createColumn({ name: "paymentmethod", label: "Payment Method" }),
                        poliza = search.createColumn({ name: "internalid", join: "CUSTRECORD_BEX_TRANCHECK", label: "Internal ID" }),
                    ]
            });
            var searchResultCount = requestforquoteSearchObj.runPaged().count;
            log.debug("requestforquoteSearchObj result count", searchResultCount);

            var myResults = getAllResults(requestforquoteSearchObj);
            myResults.forEach(function (result) {
                let vals = result.getAllValues()
                info.push({
                    type: result.getValue(type),
                    tranid: result.getValue(tranid),
                    transactionnumber: result.getValue(transactionnumber),
                    custbody_bex_operation_number: result.getValue(custbody_bex_operation_number),
                    trandate: result.getValue(trandate),
                    location: result.getText(loc),
                    department: result.getText(department),
                    altname: result.getValue(altname),
                    custentity_bex_rfc_taxid_nif: result.getValue(custentity_bex_rfc_taxid_nif),
                    statusTran: result.getValue(statusTran),
                    city: result.getValue(city),
                    productDescription: result.getValue(productDescription),
                    productType: result.getValue(productType),
                    genericKey: result.getValue(genericKey),
                    graduation: 'Null',
                    quantity: result.getValue(quantity),
                    units: result.getValue(units),
                    subtotal: result.getValue(subtotal),
                    iva: -(result.getValue(iva)),
                    tasaIeps: result.getValue(tasaIeps),
                    ieps: result.getValue(ieps),
                    discount: result.getValue(discount),
                    ivadiscount: result.getValue(ivadiscount),
                    iepsdiscount: result.getValue(iepsdiscount),
                    netofactura: result.getValue(netofactura),
                    fechapago: result.getValue(fechapago),
                    numerocheque: result.getValue(numerocheque),
                    estatus: result.getValue(estatus),
                    tipopago: result.getValue(tipopago),
                    poliza: result.getValue(poliza),
                });
            });

            // requestforquoteSearchObj.run().each(function (result) {
            //     // .run().each has a limit of 4,000 results
            //     let vals = result.getAllValues()
            //     info.push({
            //         type: result.getValue(type),
            //         tranid: result.getValue(tranid),
            //         transactionnumber: result.getValue(transactionnumber),
            //         custbody_bex_operation_number: result.getValue(custbody_bex_operation_number),
            //         trandate: result.getValue(trandate),
            //         location: result.getText(loc),
            //         department: result.getText(department),
            //         altname: result.getValue(altname),
            //         custentity_bex_rfc_taxid_nif: result.getValue(custentity_bex_rfc_taxid_nif),
            //         statusTran: result.getValue(statusTran),
            //         city: result.getValue(city),
            //         productDescription: result.getValue(productDescription),
            //         productType: result.getValue(productType),
            //         genericKey: result.getValue(genericKey),
            //         graduation: 'Null',
            //         quantity: result.getValue(quantity),
            //         units: result.getValue(units),
            //     });
            //     return true;
            // });

            log.debug('busqueda', info)

            return info;
        }

        function getAllResults(s) {
            var results = s.run();
            var searchResults = [];
            var searchid = 0;
            do {
                var resultslice = results.getRange({ start: searchid, end: searchid + 1000 });
                resultslice.forEach(function (slice) {
                    searchResults.push(slice);
                    searchid++;
                }
                );
            } while (resultslice.length >= 1000);
            return searchResults;
        }

    });
